% Klasse for dokumenter utgitt av Matematisk fagutvalg
% Implementert av Martin Helsø (martibhe@math.uio.no)
% Versjon 08-08-2015

\ProvidesPackage{MFU}
\LoadClass[a4paper, norsk]{article}

\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage{lmodern}
\RequirePackage[norsk]{babel}
\RequirePackage{microtype}
\RequirePackage{url}
\RequirePackage{graphicx}
\RequirePackage{pdfpages}
\RequirePackage{textcomp}
\RequirePackage{enumerate}
\RequirePackage{csquotes}
\RequirePackage{xcolor}
\RequirePackage[colorlinks, 
                linkcolor = MFUfarge,
                urlcolor  = MFUfarge,
                citecolor = MFUfarge]
                {hyperref}
\RequirePackage[margin = 20mm, 
                bottom = 1.6in, 
                top    = 1.5in]
               {geometry}
\RequirePackage{tabularx}
\RequirePackage[absolute]{textpos}
\RequirePackage{everypage}
\RequirePackage{sectsty}

\graphicspath{{MFU-bilder/}}
\urlstyle{sf}
\allsectionsfont{\sffamily}

\DeclareOption{kontaktinformasjon}
{
    \AddEverypageHook
    {
        \begin{textblock*}{105mm}(49mm, 271mm)
            \tiny 
            \sffamily
            \begin{tabularx}{104mm}[t]
                {
                    @{} 
                    >{\raggedright}X@{} 
                    >{\raggedright}X@{} 
                    >{\raggedright}X@{} 
                }
                {\bfseries Matematisk fagutvalg} \\[1ex] 
                \href{mailto:fagutvalg-liste@math.uio.no}
                     {fagutvalg-liste@math.uio.no}
                &
                {\bfseries Bes\o ksadresse} \\[1ex] 
                UE16 \\
                Moltke Moes vei 35 \\ 
                Niels Henrik Abels hus \\
                0851 OSLO 
                &
                {\bfseries Postadresse} \\[1ex]
                Postboks 1053 Blindern \\
                0316 OSLO
            \end{tabularx}
        \end{textblock*}
    }
}

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions*

\renewcommand{\sfdefault}{phv}

\AddEverypageHook
{
    \begin{textblock*}{15cm}(20mm, 12mm)
        \includegraphics[height = 9.3mm]{MI-logo}
    \end{textblock*}
    \begin{textblock*}{20mm}(22.5mm, 105mm)
        \includegraphics{UiO-kolon}
    \end{textblock*}    
    \begin{textblock*}{19mm}(24mm, 266mm)
        \includegraphics[height=19mm]{UiO-segl}
    \end{textblock*}
    \begin{textblock*}{19mm}(148mm, 266mm)
        \includegraphics[height = 19mm]{MFU-logo}
    \end{textblock*}
}

\renewcommand{\maketitle}
{
    \newpage
    \null
    \begin{flushleft}
        {\LARGE \bfseries \sffamily \@title \par}
        \vskip 1em
        {\large \bfseries \@author \par}
        \vskip 0.5em
        {\large \@date}
    \end{flushleft}
    \par
    \vskip 1.5em
}

\author{Matematisk fagutvalg}
\pagenumbering{gobble}
\setlength{\leftmargini}{13pt}
\definecolor{MFUfarge}{rgb}{0, 0, 0.55}

\endinput